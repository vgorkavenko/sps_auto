import org.junit.*;


import org.openqa.selenium.By;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


/*
        Город вылета, город прилета ГОТОВО
        Установлена даты вылета туда (текущая дата) ГОТОВО
        Значения по умолчанию не изменяем ОК
        Переход к поиску авиабилетов ГОТОВО
        Отображение списка авиаперелетов + СВАЙП ГОТОВО
        Переход в конкретный билет + Проверка того что мы перешли в билет на который жали (по цене) ГОТОВО
*/

public class AviaTest extends MainClass {

    @Test
    public void AviaWindow_NavigationBarTitle()
    {
        RestartApp();
        NavigateUp();
        By travel = By.xpath("//android.widget.TextView[@text='Путешествия']");
        By avia = By.xpath("//android.widget.TextView[@text='Авиабилеты']");
        SwipeForFindElement(travel);
        driver.findElement(travel).click();
        driver.label("Menu");
        SwipeForFindElement(avia);
        driver.findElement(avia).click();
        Timeout(3);
        CloseEducationWindow();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Авиабилеты']"));
        driver.label("Avia");
    }

    @Test
    public void AviaWindow_CitySetFrom() throws Exception {
        RestartApp();
        NavigateUp();
        By travel = By.xpath("//android.widget.TextView[@text='Путешествия']");
        By avia = By.xpath("//android.widget.TextView[@text='Авиабилеты']");
        SwipeForFindElement(travel);
        driver.findElement(travel).click();
        driver.label("Menu");
        SwipeForFindElement(avia);
        driver.findElement(avia).click();
        Timeout(3);
        CloseEducationWindow();

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/cityFrom")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        FinderFromObjList("MOW","Москва");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/city")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Москва']"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='MOW']"));
        driver.label("City set From");
    }

    @Test
    public void AviaWindow_CitySetInto() throws Exception {
        RestartApp();
        NavigateUp();
        By travel = By.xpath("//android.widget.TextView[@text='Путешествия']");
        By avia = By.xpath("//android.widget.TextView[@text='Авиабилеты']");
        SwipeForFindElement(travel);
        driver.findElement(travel).click();
        driver.label("Menu");
        SwipeForFindElement(avia);
        driver.findElement(avia).click();
        Timeout(3);
        CloseEducationWindow();

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/cityTo")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        FinderFromObjList("LON","Лондон");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/city")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Лондон']"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='LON']"));
        driver.label("City set into");
    }

    @Test
    public void AviaWindow_DateDefault() throws Exception {
        RestartApp();
        NavigateUp();
        By travel = By.xpath("//android.widget.TextView[@text='Путешествия']");
        By avia = By.xpath("//android.widget.TextView[@text='Авиабилеты']");
        SwipeForFindElement(travel);
        driver.findElement(travel).click();
        driver.label("Menu");
        SwipeForFindElement(avia);
        driver.findElement(avia).click();
        Timeout(3);
        CloseEducationWindow();

        Date currentDate = new Date();
        SimpleDateFormat formatForDay = new SimpleDateFormat("dd");
        String s = formatForDay.format(currentDate);

        String dateFrom = driver.findElement(By.id("ru.sberbank.spasibo.test:id/dateFromDate")).getText();

        if(!(s.equals(dateFrom.substring(0,2))))
            driver.findElement(By.id("Not equals!"));
    }

    @Test
    public void AviaWindow_SearchAviaButtonClick() throws Exception {
        RestartApp();
        NavigateUp();
        By travel = By.xpath("//android.widget.TextView[@text='Путешествия']");
        By avia = By.xpath("//android.widget.TextView[@text='Авиабилеты']");
        SwipeForFindElement(travel);
        driver.findElement(travel).click();
        driver.label("Menu");
        SwipeForFindElement(avia);
        driver.findElement(avia).click();
        Timeout(3);
        CloseEducationWindow();

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/cityFrom")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        FinderFromObjList("MOW","Москва");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/city")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.label("City set From");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/cityTo")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        FinderFromObjList("LON","Лондон");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/city")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.label("City set into");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/searchButton")).click();
        driver.label("Search Button Is Clicked");
    }

    @Test
    public void AviaWindow_SearchListSomethingNavigateBarTitle() throws Exception {
        RestartApp();
        NavigateUp();
        By travel = By.xpath("//android.widget.TextView[@text='Путешествия']");
        By avia = By.xpath("//android.widget.TextView[@text='Авиабилеты']");
        SwipeForFindElement(travel);
        driver.findElement(travel).click();
        driver.label("Menu");
        SwipeForFindElement(avia);
        driver.findElement(avia).click();
        Timeout(3);
        CloseEducationWindow();

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/cityFrom")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        FinderFromObjList("MOW","Москва");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/city")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.label("City set From");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/cityTo")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        FinderFromObjList("LON","Лондон");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/city")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.label("City set into");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/searchButton")).click();
        driver.label("Search Button Is Clicked");

        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/action_bar"));
    }


    @Test
    public void AviaWindow_SearchListSwipe() throws Exception {
        RestartApp();
        NavigateUp();
        By travel = By.xpath("//android.widget.TextView[@text='Путешествия']");
        By avia = By.xpath("//android.widget.TextView[@text='Авиабилеты']");
        SwipeForFindElement(travel);
        driver.findElement(travel).click();
        driver.label("Menu");
        SwipeForFindElement(avia);
        driver.findElement(avia).click();
        Timeout(3);
        CloseEducationWindow();

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/cityFrom")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        FinderFromObjList("MOW","Москва");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/city")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.label("City set From");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/cityTo")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        FinderFromObjList("LON","Лондон");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/city")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.label("City set into");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/searchButton")).click();
        driver.label("Search Button Is Clicked");

        By downObj = By.xpath("//android.widget.TextView[@text='23:55 - ']");
        By upObj = By.xpath("//android.widget.TextView[@text='20:45 - ']");

        Timeout(8);
        SwipeForFindElement(downObj);
        driver.label("AviaList down swipe obj");
        Timeout(3);
        SwipeUpForFindElement(upObj);
        driver.label("AviaList up swipe obj");
    }


    @Test
    public void AviaWindow_SearchListGoToItem() throws Exception {
        RestartApp();
        NavigateUp();
        By travel = By.xpath("//android.widget.TextView[@text='Путешествия']");
        By avia = By.xpath("//android.widget.TextView[@text='Авиабилеты']");
        SwipeForFindElement(travel);
        driver.findElement(travel).click();
        driver.label("Menu");
        SwipeForFindElement(avia);
        driver.findElement(avia).click();
        Timeout(3);
        CloseEducationWindow();

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/cityFrom")).click();
        Timeout(3);
        FinderFromObjList("MOW","Москва");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/city")).click();
        Timeout(2);
        driver.label("City set From");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/cityTo")).click();
        Timeout(3);
        FinderFromObjList("LON","Лондон");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/city")).click();
        Timeout(2);
        driver.label("City set into");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/searchButton")).click();
        driver.label("Search Button Is Clicked");


        driver.manage().timeouts().implicitlyWait(25,TimeUnit.SECONDS);
        By currItem = By.id("ru.sberbank.spasibo.test:id/yellowIconText");
        String currPrice = driver.findElement(currItem).getText();


        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/view_cheapest")).click();
        driver.label("Go to cheapest avia ticket");

        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'"+currPrice+"')]"));


    }
}
