
import org.junit.*;

import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import java.sql.Time;
import java.util.concurrent.TimeUnit;



public class LoadScreenTest extends MainClass {


    @Test
    public void LoadScreen_SplashScreenImageView()
    {
        driver.closeApp();
        driver.launchApp();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/splash_screen_image_view"));
        driver.label("Splash screen image");
    }

    @Test
    public void LoadScreen_TextView()
    {
        driver.closeApp();
        driver.launchApp();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/messageText"));
        driver.label("Splash screen text");
    }

    @Test
    public void LoadScreen_ProgressBarView()
    {
        driver.closeApp();
        driver.launchApp();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/progress"));
        driver.label("Splash screen progress bar");
    }

    @Test
    public void LoadScreen_LoadAppToRegistrationWindow()
    {
        RestartApp();
    }
}
