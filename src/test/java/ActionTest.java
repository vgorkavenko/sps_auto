
import org.junit.*;
import org.junit.runners.MethodSorters;

import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


@FixMethodOrder(MethodSorters.NAME_ASCENDING) //вызов тестов по алфавиту
//@FixMethodOrder(MethodSorters.DEFAULT)
//@FixMethodOrder(MethodSorters.JVM)

public class ActionTest extends MainClass{


    @Test
    public void ActionsListNavigationBarTitle()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']"));
        driver.label("Test: ActionsList");
    }

    @Test
    public void ActionsListDefault()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(5);
        By Sorter = By.id("ru.sberbank.spasibo.test:id/ivPartnerPopular");
        By FilterCounter = By.id("ru.sberbank.spasibo.test:id/item_remove");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

      //  driver.findElements(FilterCounter).isEmpty();
        if(!isElementPresent(FilterCounter))
            driver.label("Filter counter is empty");
        driver.findElements(Sorter);
    }


    @Test
    public void ActionsListActionTitle() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.label("Test: Actions list: Action title");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/action_title"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='Action']"));

    }

    @Test
    public void ActionsListActionPeriod() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.label("Test: Actions list: Action period");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/action_period"));
        try {
            driver.findElement(By.xpath("//android.widget.TextView[@text='c 14 February 2018']"));
        } catch (Exception e)
        {
            driver.findElement(By.xpath("//android.widget.TextView[@text='c 14 февраля 2018']"));
        }

    }

    @Test
    public void ActionsListActionPercent() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.label("Test: Actions list: Action percent");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/actions_percent"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='от 4%']"));

    }

    @Test
    public void ActionsListActionIcon() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.label("Test: Actions list: Action Icon");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/action_icon"));

    }

    @Test
    public void ActionsListActionMarker() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.label("Test: Actions list: Action Icon");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/row_action_text_view_text_marker"));

    }

    @Test
    public void ActionsListActionLikeIcon() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.label("Test: Actions list: Action Like icon");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/actions_like"));

    }

    @Test
    public void ActionListFinder()  throws Exception
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();

        String whatEnter = "Action";
        String whatFind = "Action";

        FinderFromObjList(whatEnter,whatFind);

        driver.label("Test: Actions list: Action find");

    }

    



    


    @Test
    public void ActionListSortDateEnd()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        //SortType (string) - название сортировки (точное)
        //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
        String SortType = "По дате окончания";
        List<String> EthalonSortList = new ArrayList<String>();
        EthalonSortList.add("Короткое описание акции плантации");
        EthalonSortList.add("Action");

        UseSortAction(SortType, EthalonSortList);

    }

    @Test
    public void ActionListSortPopularity()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        //SortType (string) - название сортировки (точное)
        //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
        String SortType = "По популярности партнера";
        List<String> EthalonSortList = new ArrayList<String>();
        EthalonSortList.add("Action");
        EthalonSortList.add("Короткое описание акции плантации");

        UseSortAction(SortType, EthalonSortList);

    }

    @Test
    public void ActionListSortDateStart()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        //SortType (string) - название сортировки (точное)
        //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
        String SortType = "По дате начала";
        List<String> EthalonSortList = new ArrayList<String>();
        EthalonSortList.add("Action");
        EthalonSortList.add("Короткое описание акции плантации");

        UseSortAction(SortType, EthalonSortList);

    }

    @Test
    public void ActionListSortBonusCount()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        //SortType (string) - название сортировки (точное)
        //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
        String SortType = "По количеству начисляемых бонусов";
        List<String> EthalonSortList = new ArrayList<String>();
        EthalonSortList.add("Action");
        EthalonSortList.add("Короткое описание акции плантации");

        UseSortAction(SortType, EthalonSortList);

    }

    @Test
    public void ActionListSortName()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        //SortType (string) - название сортировки (точное)
        //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
        String SortType = "По партнерам по алфавиту";
        List<String> EthalonSortList = new ArrayList<String>();
        EthalonSortList.add("Короткое описание акции плантации");
        EthalonSortList.add("Action");

        UseSortAction(SortType, EthalonSortList);

    }


    @Test
    public void PersonalActionList()
    {
        RestartApp();
        UserRegistration("9001111111");
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Персональные']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.label("Test: Personal Actions list");

    }


    @Test
    public void PersonalActionListActionTitle() {

        RestartApp();
        UserRegistration("9001111111");
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Персональные']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.label("Test: Personal Actions list: Action title");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/action_title"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акция3']"));

    }

    @Test
    public void PersonalActionListActionPeriod() {

        RestartApp();
        UserRegistration("9001111111");
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Персональные']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.label("Test: Personal Actions list: Action period");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/action_period"));
        try {
            driver.findElement(By.xpath("//android.widget.TextView[@text='19 October 2017 - 30 June 2019']"));
        } catch (Exception e)
        {
            driver.findElement(By.xpath("//android.widget.TextView[@text='19 октября 2017 - 30 июня 2019']"));
        }

    }

    @Test
    public void PersonalActionListActionPercent() {

        RestartApp();
        UserRegistration("9001111111");
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Персональные']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.label("Test: Personal Actions list: Action percent");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/actions_percent"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='333']"));

    }

    @Test
    public void PersonalActionListActionIcon() {

        RestartApp();
        UserRegistration("9001111111");
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Персональные']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.label("Test: Personal Actions list: Action Icon");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/action_icon"));

    }


    @Test
    public void PersonalActionListActionLikeIcon() {

        RestartApp();
        UserRegistration("9001111111");
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Персональные']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.label("Test: Personal Actions list: Action Like icon");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/actions_like"));

    }

/*
    @Test
    public void ActionListFilterView()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();
        By categories = By.xpath("//android.widget.TextView[@text='Категории']");
        By partners = By.xpath("//android.widget.TextView[@text='Партнеры']");
        By cardtypes = By.xpath("//android.widget.TextView[@text='Типы карт']");

        driver.label("Test: Filter Action");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_option_end_date_text"));
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_start_date_text"));
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_option_bonus_text"));
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_option_partner_name_text"));
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_option_partner_popular_text"));

        SwipeForFindElement(categories);
        driver.findElement(categories).click();
        driver.label("Test: Filter Action (categories)");

        SwipeForFindElement(partners);
        driver.findElement(partners).click();
        driver.label("Test: Filter Action (partners)");

        SwipeForFindElement(cardtypes);
        driver.findElement(cardtypes).click();
        driver.label("Test: Filter Action (card types)");

    }


    @Test
    public void ActionListFilterUseCategory()
    {
        //   UseFilter(String FilterItem, List<String> ItemsName, String GreatestFilterResult, List<String> GreatestsTitles)
        //FilterItem (string) - Название параметра фильтрации (к примеру. "Категории")
        //ItemsName (list) - Названия итемов в параметре (к примеру, названия категорий в фильтре категорий). Если есть цифры в скобках, их тоже.
        //GreatestFilterResult (string) - Текст в оранжевой плашке, который мы ожидаем (к примеру, Показать 2 акции)
        //GreatestsTitles (list) - Названия эталонных элементов, по которым считаем, что фильтрация прошла правильно (к примеру, названия акций)
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Категории");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("Продукты (1)");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Action");
        String GreatestFilterResult = "Показать 1 акцию";


        UseFilter(NameFilter,ItemsName,GreatestFilterResult,GreatestsTitles);

    }



    @Test
    public void ActionListFilterUsePartners()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Партнеры");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("Седьмой Континент (1)");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Action");
        String GreatestFilterResult = "Показать 1 акцию";


        UseFilter(NameFilter,ItemsName,GreatestFilterResult,GreatestsTitles);

    }



    @Test
    public void ActionListFilterUseCardType() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Типы карт");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("MAsterCard Platinum");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Action");
        GreatestsTitles.add("Короткое описание акции плантации");
        String GreatestFilterResult = "Показать 2 акции";

        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);

    }



    @Test
    public void ActionListFilterUseMultiplyPartnerAndCategory()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Партнеры");
        NameFilter.add("Категории");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("Седьмой Континент (1)");
        ItemsName.get(0).add("омном (1)");
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(1).add("Продукты (1)");
        ItemsName.get(1).add("Строительство и ремонт (1)");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Action");
        GreatestsTitles.add("Короткое описание акции плантации");
        String GreatestFilterResult = "Показать 2 акции";

        UseFilter(NameFilter,ItemsName,GreatestFilterResult,GreatestsTitles);
    }


    @Test
    public void ActionListFilterUseMultiplyPartnerAndCardType()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Партнеры");
        NameFilter.add("Типы карт");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("омном (1)");
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(1).add("MAsterCard Platinum");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Короткое описание акции плантации");
        String GreatestFilterResult = "Показать 1 акцию";

        UseFilter(NameFilter,ItemsName,GreatestFilterResult,GreatestsTitles);
    }


    @Test
    public void ActionListFilterUseMultiplyCategoryAndCardType()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Категории");
        NameFilter.add("Типы карт");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("Продукты (1)");
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(1).add("MAsterCard Platinum");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Action");

        String GreatestFilterResult = "Показать 1 акцию";

        UseFilter(NameFilter,ItemsName,GreatestFilterResult,GreatestsTitles);
    }


    @Test
    public void ActionListUseFilterMultiplyAndGetCountFilters()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();

        Timeout(9);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Партнеры");
        NameFilter.add("Категории");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("Седьмой Континент (1)");
        ItemsName.get(0).add("омном (1)");
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(1).add("Продукты (1)");
        ItemsName.get(1).add("Строительство и ремонт (1)");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Action");
        GreatestsTitles.add("Короткое описание акции плантации");
        String GreatestFilterResult = "Показать 2 акции";

        UseFilter(NameFilter,ItemsName,GreatestFilterResult,GreatestsTitles);

        if(driver.findElement(By.id("ru.sberbank.spasibo.test:id/icon_counter")).getText().equals("2"))
            driver.label("Filter counter is equals");
    }

      @Test
    public void ActionsListClearFinderAfterUseFilter()  throws Exception
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Акции']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        String whatEnter = "Action";
        String whatFind = "Action";

        FinderFromObjList(whatEnter,whatFind);

        driver.label("Test: Actions list: Find is filled");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_filter")).click();
        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Категории");
        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("Продукты (1)");
        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Action");
        String GreatestFilterResult = "Показать 1 акцию";
        UseFilter(NameFilter,ItemsName,GreatestFilterResult,GreatestsTitles);

        if(driver.findElement(By.id("ru.sberbank.spasibo.test:id/edittext")).getText().equals(whatEnter))
            driver.findElement(By.id("Test: Events list: Error: Find is filled!"));
        else
            driver.label("Test: Events list: Find is not filled after use filter");
    }
*/

    //--------------------------------------------------------------------------------------------------------------------------------------------------------

    //Использование конкретного параметра сортировки акций
    private void UseSortAction(String SortType, List<String> EthalonSortList)
    //SortType (string) - название сортировки (точное)
    //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
    {
        By SortItem = By.xpath("//android.widget.TextView[@text='" + SortType + "']");
        SwipeForFindElement(SortItem);
        driver.findElement(SortItem).click();
        NavigateUp();
        for(int i=0; i<EthalonSortList.size(); i++)
        {
            By currItem =  By.xpath("//android.widget.RelativeLayout[@index='"+i+"']/android.widget.LinearLayout/" +
                    "android.widget.FrameLayout/" +
                    "android.widget.LinearLayout/" +
                    "android.widget.LinearLayout/" +
                    "android.widget.TextView[@text='"+EthalonSortList.get(i)+"']");
            SwipeForFindElement(currItem);
            driver.findElement(currItem);
        }
    }
}



