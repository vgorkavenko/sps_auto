import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

import org.junit.*;
import org.junit.rules.TestWatcher;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.NoSuchElementException;


import com.microsoft.appcenter.appium.Factory;
import com.microsoft.appcenter.appium.EnhancedAndroidDriver;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.net.URL;

public class MainClass {

    public static EnhancedAndroidDriver<MobileElement> driver;

    @Rule
    public TestWatcher watcher = Factory.createWatcher();

    @BeforeClass
    public static void CreateDriver() throws Exception {
        System.out.println("BeforeClass. Create driver");

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8.1.0");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "pixel_xl");
        //      capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "52008969b67874a5");
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UIAutomator2");

        capabilities.setCapability("app", "C:\\sp.apk");
        capabilities.setCapability("timeoutInSeconds", 80);
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 120);


//        capabilities.setCapability("autoAcceptAlerts", "true");

        driver = Factory.createAndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

//        WebDriverWait wait;
//        wait = new WebDriverWait(driver,180);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }




    public void Timeout(int sec)
    {
        try {
            Thread.sleep(sec*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void RestartApp()
    {
       // driver.closeApp();
      //  driver.launchApp();
        driver.resetApp();
        /*
        System.out.println("City pop-ups.");
        try {
            if (driver.findElements(By.xpath("//android.widget.TextView[contains(@text,'Ваш город')]")).size() > 0) {
                System.out.println("City found popup? NO click");
                driver.findElement(By.id("android:id/button2")).click();
            }
            if (driver.findElements(By.xpath("//android.widget.TextView[contains(@text,'Не удалось найти')]")).size() > 0) {
                System.out.println("City not found popup? OK click");
                driver.findElement(By.id("android:id/button1")).click();
            } else
                System.out.println("City pop-ups not found!");
        } catch (Exception e){System.out.println("City pop-ups not found!");}
*/
        driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Выбор города']"));
        System.out.println("Region and City select. Swipe.");
        //  By cityRegion = By.xpath("//android.widget.TextView[@text='Якутс']");
        By myCity = By.xpath("//android.widget.TextView[@text='Москва']");
        //   driver.label("02 Regions");
        //    SwipeForFindElement(cityRegion);
        //    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        //     driver.findElement(cityRegion).click();
        driver.label("City");
        SwipeForFindElement(myCity);
      //  driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(myCity).click();

        driver.label("Notification pop-up");

        if (driver.findElements(By.xpath("//android.widget.TextView[contains(@text,'Разрешение уведомлений')]")).size() > 0 ) {
            System.out.println("Enable notification");
            driver.findElement(By.id("android:id/button2")).click();
        }

        driver.label("Lerning screen.");
        NavigateUp();
    }

    public void UserRegistration(String number)
    {
        System.out.println("CheckBox. Registration number.");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        Timeout(1);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue(number);
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }

        System.out.println("Registration date.");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        Timeout(1);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        Timeout(1);
        driver.label("Registration.");
        System.out.println("Registration finish.");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();

        System.out.println("Registration popup.");
        driver.findElement(By.id("android:id/button2")).click();
        Timeout(1);
        System.out.println("SMS code enter.");
        driver.label("SMS");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/code")).sendKeys("1234");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/confirm")).click();

        System.out.println("Security code creation.");
        Timeout(1);
        driver.label("Security code");
        driver.findElement(By.xpath("//android.widget.TextView[@text='1']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='2']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='3']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='4']")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/action")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Введите защитный код еще раз:']"));

        System.out.println("Security code repeat.");
        Timeout(1);
        driver.findElement(By.xpath("//android.widget.TextView[@text='1']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='2']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='3']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='4']")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/action")).click();


        try {
            if (driver.findElements(By.xpath("//android.widget.TextView[contains(@text,'Использовать Touch ID')]")).size() > 0) {
                driver.findElement(By.id("ru.sberbank.spasibo.test:id/negative")).click();
            }
            else
                System.out.println("Touch ID pop-up not found!");
        } catch (Exception e){System.out.println("Touch ID pop-up not found!");}


        driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Мой баланс:']"));
    }

    public  void CloseEducationWindow()
    {
        try {
            driver.findElement(By.id("ru.sberbank.spasibo.test:id/close")).click();
            System.out.println("Education window closed");
        } catch (Exception e)
        {
            System.out.println("Error: Education window not found!");
        }
    }


    public void NavigateUp()
    {
        Timeout(2);
        if (driver.findElements(By.xpath("//*[contains(@content-desc,'Navigate up')]")).size() > 0) {
            driver.findElement(By.xpath("//*[contains(@content-desc,'Navigate up')]")).click();
        }
        else {
            System.out.println("NavigateUp or Переход вверх not found! Try again");
        }
        if (driver.findElements(By.xpath("//*[contains(@content-desc,'Переход вверх')]")).size() > 0) {
            driver.findElement(By.xpath("//*[contains(@content-desc,'Переход вверх')]")).click();
        } else {
            System.out.println("NavigateUp or Переход вверх not found! Try again");
        }
    }

    public void SwipeForFindElement(By element)
    {
        Boolean isFoundElement;
        Dimension size = driver.manage().window().getSize();
        int startPoint = (int) (size.height * 0.8);
        int endPoint = (int) (size.height * 0.2);
        int leftPoint = (int) (size.width * 0.5);

        driver.manage().timeouts().implicitlyWait(1,TimeUnit.SECONDS);

        isFoundElement = driver.findElements(element).size() > 0;
        int tmp = 0;
        while (!isFoundElement && tmp<50){
            driver.swipe(leftPoint,startPoint,leftPoint,endPoint, 800);
            isFoundElement = driver.findElements(element).size() > 0;
            tmp++;
        }
    }

    public void SwipeUpForFindElement(By element)
    {
        Boolean isFoundElement;
        Dimension size = driver.manage().window().getSize();
        int startPoint = (int) (size.height * 0.8);
        int endPoint = (int) (size.height * 0.4);
        int leftPoint = (int) (size.width * 0.5);

        driver.manage().timeouts().implicitlyWait(1,TimeUnit.SECONDS);

        isFoundElement = driver.findElements(element).size() > 0;
        int tmp = 0;
        while (!isFoundElement && tmp<50) {
            driver.swipe(leftPoint,endPoint,leftPoint,startPoint, 800);
            isFoundElement = driver.findElements(element).size() > 0;
            tmp++;
        }
    }

    //Использование конкретного параметра фильтрации, с списком возможных вариантов.
    public void UseFilter(List<String> FiltersName, ArrayList<ArrayList<String>> ItemsName, String GreatestFilterResult, List<String> GreatestsTitles)
    //FiltersName(list) - Название параметра фильтрации (к примеру. "Категории")
    //ItemsName (Лист листов) - Названия итемов в параметре (к примеру, названия категорий в фильтре категорий).
    //GreatestFilterResult (string) - Текст в оранжевой плашке, который мы ожидаем (к примеру, Показать 2 акции)
    //GreatestsTitles (list) - Названия эталонных элементов, по которым считаем, что фильтрация прошла правильно (к примеру, названия акций)
    {

        List<By> listFilters = new ArrayList<By>();
        for(int i =0; i<ItemsName.size(); i++)
            listFilters.add(By.xpath("//android.widget.TextView[@text='" + FiltersName.get(i) + "']"));

        ArrayList<ArrayList<By>> listItems = new ArrayList<ArrayList<By>>();
        for(int j = 0; j < FiltersName.size(); j++) {
            listItems.add(new ArrayList<By>());
            for (int i = 0; i < ItemsName.get(j).size(); i++) {
                //            driver.findElements(By.xpath("//android.widget.TextView[contains(@text,'" + ItemsName.get(j).get(i) + "']")).toString();
                listItems.get(j).add(By.xpath("//android.widget.TextView[@text='" + ItemsName.get(j).get(i) + "']"));
            }
        }


        driver.label("Test: Filter use");

        for(int j=0; j<listFilters.size();j++) {
            SwipeForFindElement(listFilters.get(j));
            driver.findElement(listFilters.get(j)).click();
            for (int i = 0; i < listItems.get(j).size(); i++) {
                SwipeForFindElement(listItems.get(j).get(i));
                driver.findElement(listItems.get(j).get(i)).click();
            }
        }

      //  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Timeout(8);
        driver.findElement(By.xpath("//android.widget.TextView[@text='"+ GreatestFilterResult +"']")).click();
      //  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Timeout(2);

        driver.label("Test: In list");

        for(int i=0; i<GreatestsTitles.size(); i++) {
            By GreatestTitle = By.xpath("//android.widget.TextView[@text='"+GreatestsTitles.get(i)+"']");
            SwipeForFindElement(GreatestTitle);
            Timeout(1);
            driver.findElement(GreatestTitle);
        }

    }



    //Метод использования строки поиска в списке
    public void FinderFromObjList(String whatEnter, String whatFind) throws Exception
    //whatEnter - что вводим в строку поиска
    //whatFind - что ожидаем получить
    {
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/edittext")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/edittext")).setValue(whatEnter);
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.label("Test: Find Item");
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='"+whatFind+"']"));
    }



    public void UsePriceEnter(String min , String max)
    {
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/minPriceText")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/minPriceText")).clear();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/minPriceText")).setValue(min);
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/maxPriceText")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/maxPriceText")).clear();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/maxPriceText")).setValue(max);
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }

        driver.label("Price enter");
    }


    //Перемещение точки (точек)
    public void UseDrag(double percentFirst, double perсentSecond)
    //percentFirst - на сколько процентов переместить первую точку
    //perсentSecond - на сколько процентов переместить вторую точку
    {

        int drag1x = driver.findElement(By.id("ru.sberbank.spasibo.test:id/drag1")).getRect().getX();
        int drag2x = driver.findElement(By.id("ru.sberbank.spasibo.test:id/drag2")).getRect().getX();

        int dragY = driver.findElement(By.id("ru.sberbank.spasibo.test:id/drag1")).getRect().getY();

        Dimension size = driver.manage().window().getSize();

        int endDrag1 =  drag1x + (int) (size.width * percentFirst);
        int endDrag2 =  drag2x - (int) (size.width * perсentSecond);

        int y = dragY;

        driver.swipe(drag1x+10,y,endDrag1,y, 800);
        Timeout(2);
        driver.swipe(drag2x,y,endDrag2,y, 800);
        driver.label("Use drag");

    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }


    @AfterClass
    public static void after() throws Exception {
        driver.quit();
    }


}



