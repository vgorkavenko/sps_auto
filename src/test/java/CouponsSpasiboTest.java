import org.junit.*;


import org.openqa.selenium.By;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


//Отображение элементов на карточке купонов в списке
//Скролл вверх-вниз

public class CouponsSpasiboTest extends MainClass {

    
    @Test
    public void CouponSpasiboListNavigationBarTitle()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны СПАСИБО']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны']"));
        driver.label("Test: CouponSpasiboList");
    }

    
    @Test
    public void CouponSpasiboListDefault()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны СПАСИБО']")).click();
        CloseEducationWindow();

        Timeout(5);
        By Sorter = By.xpath("//android.widget.TextView[contains(@text,'По популярности')]");
        By FilterCounter = By.id("ru.sberbank.spasibo.test:id/filters_counter");

        if ((driver.findElements(FilterCounter).isEmpty()) && driver.findElements(Sorter).size() > 0 )
        {
            driver.label("Test: EventsList is Default");
        }
        else {
            if(!isElementPresent(FilterCounter))
                driver.label("Filter counter is empty");
            driver.findElements(Sorter);
        }

    }

    
    @Test
    public void CouponSpasiboListSwipe()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны СПАСИБО']")).click();
        CloseEducationWindow();

        By downObj = By.xpath("//android.widget.TextView[@text='15 дней подписки на Амедиатеку']");
        By upObj = By.xpath("//android.widget.TextView[@text='100 ОКов за СПАСИБО']");

        Timeout(8);
        SwipeForFindElement(downObj);
        driver.label("CouponSpasibo down swipe obj");
        Timeout(8);
        SwipeUpForFindElement(upObj);
        driver.label("CouponSpasibo up swipe obj");
    }

    
    @Test
    public void CouponSpasiboCouponTitle()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны СПАСИБО']")).click();
        CloseEducationWindow();

        CloseEducationWindow();
        Timeout(5);
        driver.label("Test: Coupon Title");

        driver.findElement(By.xpath("//android.widget.TextView[@text='100 ОКов за СПАСИБО']"));
    }



    @Test
    public void CouponSpasiboCouponLifetimeDay()  {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны СПАСИБО']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.label("Test: CouponSpasibo lifetime day");
        String day = "242";
        String biglionLifetime =  driver.findElement(By.id("ru.sberbank.spasibo.test:id/coupon_lifetime")).getText();
        String biglionDay = biglionLifetime.substring(0,3);

        if(!(biglionDay.equals(day)))
            driver.findElement(By.id("Not equals!"));


    }

    
    @Test
    public void CouponSpasiboCouponPrice() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны СПАСИБО']")).click();
        CloseEducationWindow();

        driver.label("Test: CouponSpasibo price");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/coupon_price"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='150 СПАСИБО']"));

    }

    
    @Test
    public void CouponSpasiboCouponsCount() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны СПАСИБО']")).click();
        CloseEducationWindow();

        driver.label("Test: CouponSpasibo Count");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/coupon_number"));

    }

    
    @Test
    public void CouponSpasiboCouponsLike() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны СПАСИБО']")).click();
        CloseEducationWindow();

        driver.label("Test: CouponSpasibo Like");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/likeCountText"));
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/itemLikedIcon"));

    }

    
    @Test
    public void CouponSpasiboCouponImage() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны СПАСИБО']")).click();
        CloseEducationWindow();

        driver.label("Test: CouponSpasibo Image");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/coupon_image"));
    }




    
    @Test
    public void CouponSpasiboGoToItem()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны СПАСИБО']")).click();
        CloseEducationWindow();

        Timeout(8);
        String txt = driver.findElement(By.id("ru.sberbank.spasibo.test:id/title")).getText();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/title")).click();
        Timeout(4);
        driver.findElement(By.xpath("//android.widget.TextView[@text='"+txt+"']"));
    }



}
