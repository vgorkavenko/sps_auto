
import org.junit.*;

import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

/*
 Проверка элементов на экране регистрации - ГОТОВО
• Регистрация ввод корректных данных - ГОТОВО
• Регистрация сообщения об ошибке (некорректный ввод данных) - ГОТОВО
• Сообщения об ошибках - как их воспроизветси в автотестах?
• Ввод ПИН-кода
• Создание ЗК, подтверждение ввода ЗК

Ввод телефона и д/р - Готово
Ввод кода из sms - Готово
Создание и подтверждение защитного кода
В диалог. окне Touch ID выбрать НЕТ
Переход на экран ЛК

 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class RegistrationWindowTest extends MainClass{


    @Test
    public void RegistrationWindow_Title()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Личный кабинет']"));
    }

    @Test
    public void RegistrationWindow_TextHelloTop()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Посмотреть свои бонусы, \n" +
                "если вы уже участник \n" +
                "«Спасибо от Сбербанка»']"));
    }

    @Test
    public void RegistrationWindow_TextTelephone()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Телефон при регистрации']"));
    }

    @Test
    public void RegistrationWindow_TextDate()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Дата рождения']"));
    }

    @Test
    public void RegistrationWindow_TextRulesOne()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Пользовательским соглашением')]"));
    }

    @Test
    public void RegistrationWindow_TextRulesTwo()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Политикой обработки обезличенных данных')]"));
    }

    @Test
    public void RegistrationWindow_TextFAQ()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Как стать участником']"));
    }

    @Test
    public void RegistrationWindow_TelephoneInfoIcon()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/info"));
    }

    @Test
    public void RegistrationWindow_RuleCheckBox()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box"));
    }

    @Test
    public void RegistrationWindow_RuleCheckBoxDefault() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.CheckBox[@checked='false']"));
    }

    @Test
    public void RegistrationWindow_ApplyButtonDefault()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.Button[@enabled='false']"));
    }

    @Test
    public void RegistrationWindow_FAQIco()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_image_view_member_arrow"));
    }

    @Test
    public void RegistrationWindow_TelephoneBox()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone"));
    }

    @Test
    public void RegistrationWindow_TelephoneBoxDefaultText()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.EditText[@text='+7              ']"));
    }

    @Test
    public void RegistrationWindow_DateBoxDefaultText()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.Button[@text='ДД.ММ.ГГГГ']"));
        //LOL: поле ввода даты - это не поле я кнопка
    }

    @Test
    public void RegistrationWindow_TelephoneInfoIconUse()
    {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/info")).click();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Для авторизации в приложении необходим номер мобильного телефона, указанный Вами при регистрации в программе. На указанный номер будет отправлено SMS сообщение с кодом.']"));
    }

    @Test
    public void RegistrationWindow_TelephoneInfoIconUseAndClose() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/info")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/close")).click();
    }

    @Test
    public void RegistrationWindow_TelephoneEnterIntoBox() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
    }

    @Test
    public void RegistrationWindow_DateEnterIntoBox() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
    }

    @Test
    public void RegistrationWindow_RuleCheckBoxUse() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.xpath("//android.widget.CheckBox[@checked='true']"));
    }

    @Test
    public void RegistrationWindow_ApplyButtonIsDisabledIfTelephoneEnteredOnly() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.findElement(By.xpath("//android.widget.Button[@enabled='false']"));
    }

    @Test
    public void RegistrationWindow_ApplyButtonIsDisabledIfDateEnteredOnly() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.findElement(By.xpath("//android.widget.Button[@enabled='false']"));
    }

    @Test
    public void RegistrationWindow_ApplyButtonIsDisabledIfRuleCheckboxCheckedOnly() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.xpath("//android.widget.Button[@enabled='false']"));
    }


    @Test
    public void RegistrationWindow_ApplyButtonIsDisabledIfTelephoneAndDateEntered() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.findElement(By.xpath("//android.widget.Button[@enabled='false']"));
    }

    @Test
    public void RegistrationWindow_ApplyButtonIsDisabledIfTelephoneEnteredAndCheckboxChecked() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.xpath("//android.widget.Button[@enabled='false']"));
    }

    @Test
    public void RegistrationWindow_ApplyButtonIsDisabledIfDateEnteredAndCheckboxChecked() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.xpath("//android.widget.Button[@enabled='false']"));
    }

    @Test
    public void RegistrationWindow_ApplyButtonIsEnabled() {
        RestartApp();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("0000000000");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.Button[@enabled='true']"));
    }

    @Test
    public void RegistrationWindow_ErrorMessageAfterApplyButtonUse() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("0000000000");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Пожалуйста, убедитесь в корректности введенных данных')]"));
    }

    @Test
    public void RegistrationWindow_ErrorMessageAfterApplyButtonUseRepeatTelephone() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("0000000000");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button1")).click();
    }

    @Test
    public void RegistrationWindow_CloseErrorMessageAfterApplyButtonUse() {
        RestartApp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("0000000000");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/close")).click();

    }

    @Test
    public void RegistrationWindow_ApplyButtonUse() {
        RestartApp();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    //    driver.findElement(By.xpath("//android.widget.TextView[@text='Подтверждение']"));
    }

    @Test
    public void SmsWindow_WindowTitle() {
        RestartApp();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Подтверждение']"));
    }

    @Test
    public void SmsWindow_LabelTxt() {
        RestartApp();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Вам отправлено SMS-сообщение с кодом подтверждения. Пожалуйста, введите его:']"));
    }

    @Test
    public void SmsWindow_RepeatSmsTxt() {
        RestartApp();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Повторить отправку SMS c кодом']"));
    }

    @Test
    public void SmsWindow_RepeatSmsErrorTxt() {
        RestartApp();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Повторить отправку SMS c кодом']")).click();
        Timeout(2);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Повторить отправку SMS c кодом']")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Следующая попытка ввода данных будет доступна через 2 минуты. Пожалуйста, подождите']"));
    }

    @Test
    public void SmsWindow_CloseRepeatSmsError() {
        RestartApp();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        } catch (Exception e) {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Повторить отправку SMS c кодом']")).click();
        Timeout(2);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Повторить отправку SMS c кодом']")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/close")).click();
    }

    @Test
    public void SmsWindow_ApplyButtonErrorText_EnterPass() {
        RestartApp();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/confirm")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Введите пароль для подтверждения']"));
    }


    @Test
    public void SmsWindow_ApplyButtonClose_EnterPassError() {
        RestartApp();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/confirm")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
       // driver.findElement(By.xpath("//android.widget.TextView[@text='Введите пароль для подтверждения']"));
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/close")).click();
    }

    @Test
    public void SmsWindow_ApplyButtonUse() {
        RestartApp();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/fragment_user_registration_check_box")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/metPhone")).setValue("9001111111");
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnBirthday")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/okButton")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/btnNext")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("android:id/button2")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/code")).sendKeys("1234");
        Timeout(1);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/confirm")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void FullRegistration_Validate()
    {
        RestartApp();
        UserRegistration("9001111111");
    }

}
