
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.List;

@FixMethodOrder(MethodSorters.NAME_ASCENDING) //вызов тестов по алфавиту


public class BiglionTest extends MainClass {



    @Test
    public void BiglionListNavigationBarTitle()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();

        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']"));
        driver.label("Test: BiglionList");
    }

    @Test
    public void BiglionListDefault()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();

        Timeout(5);
        By Sorter = By.xpath("//android.widget.TextView[contains(@text,'По цене')]");
        By FilterCounter = By.id("ru.sberbank.spasibo.test:id/filters_counter");

        if ((driver.findElements(FilterCounter).isEmpty()) && driver.findElements(Sorter).size() > 0 )
        {
            driver.label("Test: EventsList is Default");
        }
        else {
            if(!isElementPresent(FilterCounter))
                driver.label("Filter counter is empty");
            driver.findElements(Sorter);
        }

    }

    @Test
    public void BiglionListSwipe()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();

        By downObj = By.xpath("//android.widget.TextView[@text='Новогодний банкет с развлекательной программой в ресторане «Чин Чин»']");
        By upObj = By.xpath("//android.widget.TextView[@text='Сертификаты на всё меню и напитки в пивном ресторане «Элефант»']");

        Timeout(3);
        SwipeForFindElement(downObj);
        driver.label("Biglion down swipe obj");
        Timeout(3);
        SwipeUpForFindElement(upObj);
        driver.label("Biglion up swipe obj");
    }


    @Test
    public void BiglionListOfferTitle()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();

        CloseEducationWindow();
        Timeout(5);
        driver.label("Test: Biglion Title");

        driver.findElement(By.xpath("//android.widget.TextView[@text='Сертификаты на всё меню и напитки в пивном ресторане «Элефант»']"));
    }



    @Test
    public void BiglionListOfferDay()  {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(5);
        driver.label("Test: Biglion day");
        String day = "46";
        String biglionLifetime =  driver.findElement(By.id("ru.sberbank.spasibo.test:id/tvTimeLeft")).getText();
        String biglionDay = biglionLifetime.substring(0,2);

        if(biglionDay.equals(day))
                driver.label("Biglion day is equals");


    }

    @Test
    public void BiglionListOfferFinalPrice() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(5);

        driver.label("Test: Biglion Offer final price");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tvFinalPrice"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='500 \u20BD']"));

    }

    @Test
    public void BiglionListOfferOriginalPrice() {

        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(5);

        driver.label("Test: Biglion Offer final price");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tvOriginalPrice"));
        driver.findElement(By.xpath("//android.widget.RelativeLayout/android.widget.TextView[@text='1000']"));
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tvRuble"));
        driver.findElement(By.xpath("//android.widget.LinearLayout/android.widget.TextView[@text='\u20BD']"));

    }



    @Test
    public void BiglionListOfferFinder() throws Exception
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(5);

        String whatEnter = "El";
        String whatFind = "Банкет для компании до 20 человек в ресторане El Parce";

        FinderFromObjList(whatEnter,whatFind);

        driver.label("Test: Biglion list: offer find");

    }


    


    @Test
    public void BiglionListSortPrice()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(8);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabSortLayout")).click();

        //SortType (string) - название сортировки (точное)
        //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
        String SortType = "По цене";
        List<String> EthalonSortList = new ArrayList<String>();
        EthalonSortList.add("Новогодний банкет с развлекательной программой в ресторане «Чин Чин»");
        EthalonSortList.add("Доставка фуршетного сета на выбор для компании от пекарни «Кулинаръ»");

        UseSortBiglion(SortType, EthalonSortList);

    }

    @Test
    public void BiglionListSortName()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(8);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabSortLayout")).click();

        //SortType (string) - название сортировки (точное)
        //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
        String SortType = "По названию";
        List<String> EthalonSortList = new ArrayList<String>();
        EthalonSortList.add("Банкет для компании до 20 человек в ресторане El Parce");
        EthalonSortList.add("Банкет для компании от 5 до 20 человек в ресторане «Алтай»");

        UseSortBiglion(SortType, EthalonSortList);

    }

    @Test
    public void BiglionListGoToItem()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(8);
        String txt = driver.findElement(By.id("ru.sberbank.spasibo.test:id/tvName")).getText();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tvName")).click();
        Timeout(4);
        driver.findElement(By.xpath("//android.widget.TextView[@text='"+txt+"']"));
    }

    /*
    @Test
    public void BiglionListUseFilterCategory()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(8);

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Категории");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("Рестораны и кафе");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Сертификаты на всё меню и напитки в пивном ресторане «Элефант»");
        GreatestsTitles.add("Ужин для двоих или компании в ресторане европейской кухни Brioche");
        String GreatestFilterResult = "Показать 13 купонов";

        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);


    }

    @Test
    public void BiglionListClearFinderAfterUseFilter()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(5);

        String whatEnter = "El";
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/edittext")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/edittext")).setValue(whatEnter);
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }


        driver.label("Test: Biglion list: Find is filled");

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Категории']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Рестораны и кафе']")).click();
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Показать 13 купонов']")).click();
        Timeout(5);

        if(driver.findElement(By.id("ru.sberbank.spasibo.test:id/edittext")).getText().equals(whatEnter))
            driver.findElement(By.id("Test: Events list: Error: Find is filled!"));
        else
            driver.label("Test: Events list: Find is not filled after use filter");

    }
    
    @Test
    public void BiglionListUseFilterMetro()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(8);

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Станция метро");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("Трубная");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Сертификаты на всё меню и напитки в пивном ресторане «Элефант»");

        String GreatestFilterResult = "Показать 1 купон";


        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);

    }

    
    @Test
    public void BiglionListUseFilterMultiplyCategoryAndMetro()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны на скидку']")).click();
        driver.label("Menu");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Купоны Биглион']")).click();
        Timeout(8);

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Категории");
        NameFilter.add("Станция метро");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("Рестораны и кафе");
        ItemsName.get(1).add("Трубная");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Сертификаты на всё меню и напитки в пивном ресторане «Элефант»");
        String GreatestFilterResult = "Показать 1 купон";

        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);

    }


*/


    //--------------------------------------------------------------------------------------------------------------------------------------------------------


    //Использование конкретного параметра сортировки акций
    private void UseSortBiglion(String SortType, List<String> EthalonSortList)
    //SortType (string) - название сортировки (точное)
    //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
    {
        By SortItem = By.xpath("//android.widget.TextView[@text='" + SortType + "']");
        SwipeForFindElement(SortItem);
        driver.findElement(SortItem).click();
        Timeout(3);
        for(int i=0; i<EthalonSortList.size(); i++)
        {
            By currItem =  By.xpath("//android.widget.LinearLayout[@index='"+i+"']/" +
                    "android.widget.LinearLayout/" +
                    "android.widget.TextView[@text='"+EthalonSortList.get(i)+"']");
            SwipeForFindElement(currItem);
            driver.findElement(currItem);
        }
    }


}



