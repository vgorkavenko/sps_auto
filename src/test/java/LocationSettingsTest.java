

import org.junit.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import java.util.concurrent.TimeUnit;


/*
• Выбор город при старте приложения - ГОТОВО
• Экран выбора региона - ГОТОВО
• Экран выбора населенного пункта - ГОТОВО
• Поиск региона - ГОТОВО
• Поиск населенного пункта - ГОТОВО
• Автоматическое определение города
• Изменение города из настроек - ГОТОВО

        Выбор города из ТОП8 - ГОТОВО
        Выбор региона - ГОТОВО
        Выбор города из региона - ГОТОВО
        Отображение в настройках выбранного города - ГОТОВО
*/


public class LocationSettingsTest extends MainClass {


    
    @Test
    public void SelectCityAfterStartApp()
    {
        RestartApp();
        driver.label("City select from TOP8 is successful");
    }

    
    @Test
    public void WindowRegionViews()
    {
        driver.closeApp();
        driver.launchApp();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Выбор города']"));
   //     driver.findElement(By.id("ru.sberbank.spasibo.test:id/messageText"));
        driver.findElement(By.xpath("//*[contains(@content-desc,'Поиск')]"));
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/item_region_city_list_literal_text_view_literal"));
        driver.label("Region window views is OK");
    }

    
    @Test
    public void WindowSelectRegion()
    {
        driver.closeApp();
        driver.launchApp();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        By region = By.xpath("//android.widget.TextView[@text='Хабаровский край  г.Хабаровск']");
        SwipeForFindElement(region);
        driver.label("Region is find");
        driver.findElement(region).click();
        driver.label("Region select is OK");
    }

    
    @Test
    public void WindowSubCityViews()
    {
        driver.closeApp();
        driver.launchApp();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        By region = By.xpath("//android.widget.TextView[@text='Хабаровский край  г.Хабаровск']");
        SwipeForFindElement(region);
        driver.findElement(region).click();
        Timeout(5);
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Выбор города']"));
        driver.findElement(By.xpath("//*[contains(@content-desc,'Navigate up')]"));
        driver.findElement(By.xpath("//*[contains(@content-desc,'Поиск')]"));
        driver.label("SubCity window views is OK");
    }

    
    @Test
    public void WindowSelectSubCity()
    {
        driver.closeApp();
        driver.launchApp();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        By region = By.xpath("//android.widget.TextView[@text='Хабаровский край  г.Хабаровск']");
        SwipeForFindElement(region);
        driver.findElement(region).click();
        Timeout(5);
        By subcity = By.xpath("//android.widget.TextView[@text='Новый Ургал']");
        SwipeUpForFindElement(subcity);
        driver.label("SubCity is find");
        driver.findElement(subcity).click();
        driver.label("SubCity select is OK");
    }

    
    @Test
    public void SettingsWindowAfterSelectCity()
    {
        RestartApp();
        NavigateUp();
        By settings = By.xpath("//android.widget.TextView[@text='Настройки']");
        SwipeForFindElement(settings);
        driver.findElement(settings).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Москва']"));
    }

    
    @Test
    public void ReselectCity()
    {
        RestartApp();
        NavigateUp();
        By settings = By.xpath("//android.widget.TextView[@text='Настройки']");
        SwipeForFindElement(settings);
        driver.findElement(settings).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.label("City in Settings");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Москва']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Санкт-Петербург']")).click();
        Timeout(3);
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Санкт-Петербург']"));
        driver.label("Reselect city is OK");
    }

    @Test
    public void WindowSearchRegion() throws Exception {
        driver.closeApp();
        driver.launchApp();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[contains(@content-desc,'Поиск')]")).click();
        driver.label("Search icon is clicked");
        FinderFromCityList("Test123", "Республика Алтай  г.Test123");
        driver.label("Search region is OK");
    }

    @Test
    public void WindowSearchSubCity() throws Exception {
        driver.closeApp();
        driver.launchApp();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        By region = By.xpath("//android.widget.TextView[@text='Республика Алтай  г.Test123']");
        SwipeForFindElement(region);
        driver.findElement(region).click();
        Timeout(5);
        driver.findElement(By.xpath("//*[contains(@content-desc,'Поиск')]")).click();
        FinderFromCityList("Test", "Test123");
        driver.label("Search subcity is OK");
    }

//------------------------------------------------------------------------------------------------------
    public void FinderFromCityList(String whatEnter, String whatFind) throws Exception
    //whatEnter - что вводим в строку поиска
    //whatFind - что ожидаем получить
    {
   //     driver.findElement(By.id("android:id/search_src_text")).click();
        driver.findElement(By.id("android:id/search_src_text")).setValue(whatEnter);
        try {
            driver.hideKeyboard();
        }catch (Exception e)
        {
            System.out.println("hideKeyboard");
        }
        driver.label("Test: Find Item");
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='"+whatFind+"']"));
    }

}
