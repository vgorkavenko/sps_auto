
import org.junit.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import java.util.concurrent.TimeUnit;


//@FixMethodOrder(MethodSorters.NAME_ASCENDING) //вызов тестов по алфавиту


public class PartnersTest extends MainClass{

    @Test
    public void PartnersCategoryWindowTitle()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        Timeout(2);
        driver.label("Window title is \"Партнеры\"");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']"));

    }

    @Test
    public void PartnersCategoryFilterIcon()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        Timeout(5);
        driver.label("Filter Icon is enable");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/icon_bg"));

    }

    @Test
    public void PartnersCategoryEmptyCountFilter()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        Timeout(5);
        By FilterCounter = By.id("ru.sberbank.spasibo.test:id/cont_counter");
      //  driver.findElements(FilterCounter).isEmpty();
        if(!isElementPresent(FilterCounter))
            driver.label("Partner category filter counter is empty");
    }

    @Test
    public void PartnersCategorySwipe()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        Timeout(2);

        By downObj = By.xpath("//android.widget.TextView[@text='Сотовая связь']");
        By upObj = By.xpath("//android.widget.TextView[@text='Все']");

        Timeout(3);
        SwipeForFindElement(downObj);
        driver.label("Partner category down swipe obj");
        Timeout(3);
        SwipeUpForFindElement(upObj);
        driver.label("Partner category up swipe obj");
    }

    @Test
    public void PartnersCategoryNewPartnersSomewhereIcon()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        Timeout(2);
        driver.label("Something partner category have a new partners icon");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/newPartnersTextView"));

    }

    @Test
    public void PartnersCategoryFilterIconClick()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        Timeout(5);
        driver.label("Filter Icon is clicked");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/icon_bg")).click();

    }

    @Test
    public void Partners_CategoryAll_WindowTitle()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(2);
        driver.label("Window title is \"Все партнеры\"");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все партнеры']"));
    }

    @Test
    public void Partners_CategoryAll_ItemsListSwipeForFindCategories()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(3);
        By categoryname1 = By.xpath("//android.widget.TextView[@text='Автомобили']");
        By categoryname2 = By.xpath("//android.widget.TextView[@text='Мебель']");
        By categoryname3 = By.xpath("//android.widget.TextView[@text='Рестораны']");
        SwipeForFindElement(categoryname1);
        driver.label("Category1 is here");
        SwipeForFindElement(categoryname2);
        driver.label("Category2 is here");
        SwipeForFindElement(categoryname3);
        driver.label("Category3 is here");
    }

    @Test
    public void Partners_CategoryAll_ItemsList_PartnerTitle()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(3);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Доктор Столетов']"));
    }

    @Test
    public void Partners_CategoryAll_ItemsList_PartnerLikeIcon()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(3);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/itemLikedIcon"));
    }

    @Test
    public void Partners_CategoryAll_ItemsList_PartnerLikeCount()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(3);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/likeCountText"));
    }

    @Test
    public void Partners_CategoryAll_ItemsList_PartnerCategory()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(3);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/category"));
    }

    @Test
    public void Partners_SomeCategory_ItemsList_PartnerCategory()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Автомобили']")).click();
        Timeout(5);
        if(!driver.findElement(By.id("ru.sberbank.spasibo.test:id/category")).getText().equals("Автомобили")) {
            driver.label("Partner category is not this category");
            driver.findElement(By.id("Partner category is not this category"));
        }
        else
            driver.label("Partner category is this category");
    }

    @Test
    public void Partners_CategoryAll_ItemsList_SwipePartner_BlackListIconAndText()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(5);
        SwipeFirstItemToLeft();
        driver.label("Partner is swipe");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/notInterestingButton"));
        driver.label("Blacklist icon is enable");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/notInterestingText"));
        driver.label("Blacklist text is enable");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Не интересно']"));
    }

    @Test
    public void Partners_CategoryAll_ItemsList_SwipePartner_BlackListUse()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(5);
        SwipeFirstItemToLeft();
        driver.label("Partner is swipe");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/notInterestingButton")).click();
        driver.label("Blacklist icon is clicked");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнер будет скрыт из списка']"));
        driver.label("Blacklist notification");
        driver.findElement(By.id("android:id/button2")).click();
        driver.label("I say NO!");
    }

    @Test
    public void Partners_CategoryAll_ItemsList_SwipePartner_LikeIconAndText()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(5);
        SwipeFirstItemToLeft();
        driver.label("Partner is swipe");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/likeCheckbox"));
        driver.label("Like icon is enable");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/likeText"));
        driver.label("Like text is enable");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Нравится']"));
    }

    @Test
    public void Partners_CategoryAll_ItemsList_SwipePartner_LikeUse()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(5);
        SwipeFirstItemToLeft();
        driver.label("Partner is swipe");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/likeCheckbox")).click();
        driver.label("Like icon is clicked");
        driver.findElement(By.xpath("//android.widget.LinearLayout[@index='1']/" +
                "android.widget.LinearLayout/" +
                "android.widget.LinearLayout[@index='1']/" +
                "android.widget.CheckBox[@checked='true']"));
        driver.label("Like icon is checked in swipe place");
        driver.findElement(By.xpath("//android.widget.LinearLayout[@index='0']/" +
                "android.widget.FrameLayout/" +
                "android.view.ViewGroup/" +
                "android.view.ViewGroup/" +
                "android.widget.CheckBox[@checked='true']"));
        driver.label("Like icon is checked in main partner place");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/likeCheckbox")).click();
    }


    @Test
    public void Partners_CategoryAll_ItemsList_SwipePartner_FavoriteIconAndText()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(5);
        SwipeFirstItemToLeft();
        driver.label("Partner is swipe");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/favCheckbox"));
        driver.label("Like icon is enable");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/favText"));
        driver.label("Like text is enable");
        driver.findElement(By.xpath("//android.widget.TextView[@text='В избранное']"));
    }

    @Test
    public void Partners_CategoryAll_ItemsList_SwipePartner_FavoriteUse()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(5);
        SwipeFirstItemToLeft();
        driver.label("Partner is swipe");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/favCheckbox")).click();
        driver.findElement(By.xpath("//android.widget.LinearLayout[@index='1']/" +
                "android.widget.LinearLayout/" +
                "android.widget.LinearLayout[@index='2']/" +
                "android.widget.CheckBox[@checked='true']"));
        driver.label("Favorite icon is checked in swipe place");

        Timeout(2);
        driver.findElement(By.xpath("//android.widget.LinearLayout[@index='1']/" +
                "android.widget.LinearLayout/" +
                "android.widget.LinearLayout[@index='1']/" +
                "android.widget.CheckBox[@checked='true']"));
        driver.label("Like icon is checked in swipe place after use favorite");
        driver.findElement(By.xpath("//android.widget.LinearLayout[@index='0']/" +
                "android.widget.FrameLayout/" +
                "android.view.ViewGroup/" +
                "android.view.ViewGroup/" +
                "android.widget.CheckBox[@checked='true']"));
        driver.label("Like icon is checked in main partner place after use favorite");
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/likeCheckbox")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/favCheckbox")).click();
    }


    @Test
    public void Partners_CategoryAll_ItemsList_SwipePartnerFindPicture()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Партнеры']")).click();
        driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Все']")).click();
        Timeout(5);
        SwipeFirstItemToLeft();

        By picture = By.xpath("//android.widget.LinearLayout[@index='0']/" +
                "android.widget.FrameLayout/" +
                "android.view.ViewGroup/" +
                "android.widget.ImageView");
        if(!isElementPresent(picture))
            driver.label("Partner is swipe");
    }

//---------------------------------------------------------------------------------------------------------------------



    private void SwipeFirstItemToLeft()
    {
        Dimension size = driver.manage().window().getSize();
        int yPoint = (int) (size.height * 0.3);
        int xRightPoint = (int) (size.width * 0.6);
        int xLeftPoint = (int) (size.width * 0.3);
        driver.manage().timeouts().implicitlyWait(1,TimeUnit.SECONDS);
        driver.swipe(xRightPoint,yPoint,xLeftPoint,yPoint,800);

    }
}
