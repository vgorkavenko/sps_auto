
import org.junit.*;

import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING) //вызов тестов по алфавиту
//@FixMethodOrder(MethodSorters.DEFAULT)
//@FixMethodOrder(MethodSorters.JVM)

public class EventsTest extends MainClass {

    @Test
    public void EventsListNavigationBarTitle()
    {
        RestartApp();

        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();

        CloseEducationWindow();
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']"));
        driver.label("Test: EventsList");
    }

    @Test
    public void EventsListDefault()
    {
        RestartApp();

        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();

        CloseEducationWindow();
        Timeout(5);
        By Sorter = By.xpath("//android.widget.TextView[contains(@text,'По популярности')]");
        By FilterCounter = By.id("ru.sberbank.spasibo.test:id/filters_counter");

        if ((driver.findElements(FilterCounter).isEmpty()) && driver.findElements(Sorter).size() > 0 )
        {
            driver.label("Test: EventsList is Default");
        }
        else {
            if(!isElementPresent(FilterCounter))
                driver.label("Filter counter is empty");
            driver.findElements(Sorter);
        }

    }

    @Test
    public void EventListSwipe()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        By downObj = By.xpath("//android.widget.TextView[@text='ГОЛОСА РОССИИ']");
        By upObj = By.xpath("//android.widget.TextView[@text='Ботанический сад МГУ «Аптекарский огород»']");

        Timeout(3);
        SwipeForFindElement(downObj);
        driver.label("Event down swipe obj");
        Timeout(3);
        SwipeUpForFindElement(upObj);
        driver.label("Event up swipe obj");
    }


    @Test
    public void EventsListSpecialOfferTitle()
    {
        RestartApp();

        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();

        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.label("Test: EventsList Special Offer Title");

        driver.findElement(By.xpath("//android.widget.TextView[@text='Ботанический сад МГУ «Аптекарский огород»']"));
    }



    @Test
    public void EventsListSpecialOfferPeriod() {



        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        Timeout(2);
        CloseEducationWindow();
        Timeout(9);
        driver.label("Test: EventsList Special Offer period");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/coupon_lifetime"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='13.06.2017 - 31.10.2019']"));


    }

    @Test
    public void EventsListSpecialOfferPrice() {

        RestartApp();

        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();

        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.label("Test: EventsList Special Offer price");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/coupon_price"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='от 100 СПАСИБО']"));

    }


    @Test
    public void EventsListEventTitle()
    {
        RestartApp();

        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();

        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.label("Test: EventsList Event Title");

        driver.findElement(By.xpath("//android.widget.TextView[@text='Roger Waters']"));

    }

    @Test
    public void EventsListEventPrice()
    {
        RestartApp();

        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();

        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.label("Test: EventsList Event Price");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/activity_price"));
        driver.findElement(By.xpath("//android.widget.TextView[@text='от 3 000 руб.']"));

    }

    @Test
    public void EventsListEventPeriod()
    {
        RestartApp();

        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();

        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.label("Test: EventsList Event Period");

        try {
            driver.findElement(By.xpath("//android.widget.TextView[@text='31 AUG']"));
            driver.findElement(By.xpath("//android.widget.TextView[@text='FRI 19:00']"));
        } catch (Exception e)
        {
            driver.findElement(By.xpath("//android.widget.TextView[@text='31 АВГ.']"));
            driver.findElement(By.xpath("//android.widget.TextView[@text='ПТ 19:00']"));
        }

    }

    @Test
    public void EventsListSpecialOfferFinder() throws Exception
    {
        RestartApp();

        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        String whatEnter = "StartUp Show";
        String whatFind = "StartUp Show";

        FinderFromObjList(whatEnter,whatFind);

        driver.label("Test: Events list: Special offer find");

    }


    @Test
    public void EventsListEventFinder() throws Exception
    {
        RestartApp();

        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        String whatEnter = "Roger Waters";
        String whatFind = "Roger Waters";

        FinderFromObjList(whatEnter,whatFind);

        driver.label("Test: Events list: Event find");

    }



    @Test
    public void EventListSortPopularity()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabSortLayout")).click();

        //SortType (string) - название сортировки (точное)
        //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
        String SortType = "По популярности";
        List<String> EthalonSortList = new ArrayList<String>();
        EthalonSortList.add("Ботанический сад МГУ «Аптекарский огород»");
        EthalonSortList.add("Скидки в контактный зоопарк «Погладь Енота»!");
        EthalonSortList.add("StartUp Show");
        EthalonSortList.add("Roger Waters");

        UseSortEvent(SortType, EthalonSortList);

    }

    @Test
    public void EventListSortPrice()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabSortLayout")).click();

        //SortType (string) - название сортировки (точное)
        //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
        String SortType = "По цене";
        List<String> EthalonSortList = new ArrayList<String>();
        EthalonSortList.add("Ботанический сад МГУ «Аптекарский огород»");
        EthalonSortList.add("Классика в Доме Музыки");
        EthalonSortList.add("Скидки в контактный зоопарк «Погладь Енота»!");
        EthalonSortList.add("StartUp Show");

        UseSortEvent(SortType, EthalonSortList);

    }

    @Test
    public void EventListSortDate()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabSortLayout")).click();

        //SortType (string) - название сортировки (точное)
        //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
        String SortType = "По дате";
        List<String> EthalonSortList = new ArrayList<String>();

        EthalonSortList.add("Скидки в контактный зоопарк «Погладь Енота»!");
        EthalonSortList.add("Ботанический сад МГУ «Аптекарский огород»");
        UseSortEvent(SortType, EthalonSortList);

    }



/*
    @Test
    public void EventsListClearFinderAfterUseFilter() throws Exception
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        String whatEnter = "StartUp Show";
        String whatFind = "StartUp Show";

        FinderFromObjList(whatEnter,whatFind);

        driver.label("Test: Events list: Find is filled");

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Категории']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Концерты (11)']")).click();
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Показать 11 предложений']")).click();
        Timeout(5);

        if(driver.findElement(By.id("ru.sberbank.spasibo.test:id/edittext")).getText().equals(whatEnter))
            driver.findElement(By.id("Test: Events list: Error: Find is filled!"));
        else
            driver.label("Test: Events list: Find is not filled after use filter");


    }

    @Test
    public void EventListUseFilterCategory()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Категории");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("СПЕЦПРЕДЛОЖЕНИЯ (3)");
        ItemsName.get(0).add("Концерты (11)");


        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Ботанический сад МГУ «Аптекарский огород»");
        GreatestsTitles.add("Классика в Доме Музыки");
        String GreatestFilterResult = "Показать 14 предложений";

        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);


    }


    
    @Test
    public void EventListFilterDateFilled()
    {
        RestartApp();

        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        Timeout(2);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Даты']")).click();
        Timeout(2);
        driver.findElement(By.xpath("//android.widget.TextView[@text='В эти выходные:']")).click();

        if(driver.findElements(By.xpath("//android.widget.TextView[@text='Период с:']/android.widget.TextView[@index='1']")).size() > 0)
        driver.label("'Период с:' is filled");

        else driver.findElement(By.xpath("Something not filled"));

        if(driver.findElements(By.xpath("//android.widget.TextView[@text='По:']/android.widget.TextView[@index='1']")).size() > 0)
        driver.label("'По:' is filled");

        else driver.findElement(By.xpath("Something not filled"));

    }



    
    @Test
    public void EventListUseFilterDate()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Даты");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("В эти выходные:");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Ботанический сад МГУ «Аптекарский огород»");
        GreatestsTitles.add("Скидки в контактный зоопарк «Погладь Енота»!");
        GreatestsTitles.add("StartUp Show");
        GreatestsTitles.add("Классика в Доме Музыки");

        String GreatestFilterResult = "Показать 8 предложений";

        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);

    }

    
    @Test
    public void EventListUseFilterMultiplyCategoryAndDate()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Категории");
        NameFilter.add("Даты");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("СПЕЦПРЕДЛОЖЕНИЯ (3)");
        ItemsName.get(0).add("Концерты (11)");
        ItemsName.get(1).add("В эти выходные:");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Ботанический сад МГУ «Аптекарский огород»");
        GreatestsTitles.add("Даниил Крамер");
        String GreatestFilterResult = "Показать 8 предложений";

        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);

    }

    
    @Test
    public void EventListUseFilterMultiplyCategoryAndBonus()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Категории");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("СПЕЦПРЕДЛОЖЕНИЯ (3)");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Скидки в контактный зоопарк «Погладь Енота»!");
        String GreatestFilterResult = "Показать 3 предложения";
        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Количество бонусов']")).click();
        UsePriceEnter("142","159");
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Показать 1 предложение']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Скидки в контактный зоопарк «Погладь Енота»!']"));
    }

    
    @Test
    public void EventListUseFilterMultiplyCategoryAndPrice()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Категории");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("Концерты (11)");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Классика в Доме Музыки");
        String GreatestFilterResult = "Показать 11 предложений";


        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);


        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Стоимость, руб']")).click();
        UsePriceEnter("100","1758");
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Показать 1 предложение']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Классика в Доме Музыки']"));
    }

    
    @Test
    public void EventListUseFilterMultiplyDateAndBonus()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Даты");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("В эти выходные:");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Ботанический сад МГУ «Аптекарский огород»");
        GreatestsTitles.add("Скидки в контактный зоопарк «Погладь Енота»!");
        GreatestsTitles.add("StartUp Show");
        GreatestsTitles.add("Классика в Доме Музыки");

        String GreatestFilterResult = "Показать 8 предложений";

        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Количество бонусов']")).click();
        UsePriceEnter("142","159");
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Показать 1 предложение']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Скидки в контактный зоопарк «Погладь Енота»!']"));


    }


    
    @Test
    public void EventListUseFilterMultiplyDateAndPrice()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Даты");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("В эти выходные:");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Ботанический сад МГУ «Аптекарский огород»");
        GreatestsTitles.add("Скидки в контактный зоопарк «Погладь Енота»!");
        GreatestsTitles.add("StartUp Show");
        GreatestsTitles.add("Классика в Доме Музыки");

        String GreatestFilterResult = "Показать 8 предложений";

        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Стоимость, руб']")).click();
        UsePriceEnter("100","1758");
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Показать 5 предложений']")).click();
        Timeout(3);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Классика в Доме Музыки']"));

    }

    
    @Test
    public void EventListUseFilterMultiplyAndGetCountFilters()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        List<String> NameFilter = new ArrayList<String>();
        NameFilter.add("Категории");
        NameFilter.add("Даты");

        ArrayList<ArrayList<String>> ItemsName = new ArrayList<ArrayList<String>>();
        ItemsName.add(new ArrayList<String>());
        ItemsName.add(new ArrayList<String>());
        ItemsName.get(0).add("СПЕЦПРЕДЛОЖЕНИЯ (3)");
        ItemsName.get(0).add("Концерты (11)");
        ItemsName.get(1).add("В эти выходные:");

        List<String> GreatestsTitles = new ArrayList<String>();
        GreatestsTitles.add("Ботанический сад МГУ «Аптекарский огород»");
        GreatestsTitles.add("Андрей Максимов \"Диалоги при свидетелях\"");
        String GreatestFilterResult = "Показать 5 предложений";


        UseFilter(NameFilter, ItemsName, GreatestFilterResult, GreatestsTitles);

        if(!driver.findElement(By.id("ru.sberbank.spasibo.test:id/filters_counter")).getText().equals("2"))
            driver.findElement(By.id("Not equals!"));
    }

    
    @Test
    public void EventListUseFilterBonusDrag()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Количество бонусов']")).click();

        UseDrag(0.1, 0.4);

    }

    
    @Test
    public void EventListUseFilterPriceDrag()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Стоимость, руб']")).click();

        UseDrag(0.3, 0.1);

    }


    
    @Test
    public void EventListUseFilterBonusEnter()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Количество бонусов']")).click();

        UsePriceEnter("120","150");

        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Показать 1 предложение']")).click();
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Скидки в контактный зоопарк «Погладь Енота»!']"));

    }

    
    @Test
    public void EventListUseFilterPriceEnter()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();


        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Стоимость, руб']")).click();

        UsePriceEnter("100","300");

        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Показать 3 предложения']")).click();
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Классика в Доме Музыки']"));

    }

    
    @Test
    public void EventListFilterBonusIsThere()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();
        Timeout(5);

        // By SpecialOffer = By.xpath("//android.widget.TextView[contains(@text,'СПАСИБО')]");

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        Timeout(2);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Категории']")).click();
        By SpecialOffer = By.xpath("//android.widget.TextView[contains(@text,'СПЕЦПРЕДЛОЖЕНИЯ')]");
        if (driver.findElements(SpecialOffer).size() > 0 )
        {
            driver.label("Special Offers found!");
        }
        else
        {
            driver.label("Special Offers not found!");
        }

        By categoryName = By.xpath("//android.widget.TextView[@text='Количество бонусов']");
        SwipeForFindElement(categoryName);
    }

    
    @Test
    public void EventsListFilterDateCalendarCurrentDay()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();
        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Даты']")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Период с:']")).click();
        driver.findElement(By.id("android:id/button1")).click();
        Date currentDate = new Date();
        SimpleDateFormat formatForDay = new SimpleDateFormat("dd");
        String s = formatForDay.format(currentDate);

        String dateFrom = driver.findElement(By.id("ru.sberbank.spasibo.test:id/dateFromText")).getText();
        String dateTo = driver.findElement(By.id("ru.sberbank.spasibo.test:id/dateToText")).getText();

        if(!(s.equals(dateFrom.substring(0,2)) && s.equals(dateTo.substring(0,2))))
            driver.findElement(By.id("Not equals!"));


    }

    
    @Test
    public void EventListUseFilterBonusDragUse()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        Timeout(1);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Количество бонусов']")).click();

        UseDrag(0.1, 0.4);
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Показать 1 предложение']")).click();
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Скидки в контактный зоопарк «Погладь Енота»!']"));

    }


    
    @Test
    public void EventListUseFilterPriceDragUse()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Стоимость, руб']")).click();

        UseDrag(0.2, 0.4);

        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Показать 5 предложений']")).click();
        Timeout(5);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Классика в Доме Музыки']"));

    }


    
    @Test
    public void EventListUseFilterBonusMyBalance()
    {
        RestartApp();
        UserRegistration("9001111111");
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();
        Timeout(1);
        driver.findElement(By.xpath("//android.widget.TextView[@text='Количество бонусов']")).click();

        Timeout(1);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/balance"));


    }

    @Test
    public void EventListUseFilterBonusMyBalanceUseCheckBox()
    {
        RestartApp();
        UserRegistration("9001111111");
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Количество бонусов']")).click();

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/balanceFilterMyBalanceChbx")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/showCouponsButton"));
    }

    
    @Test
    public void EventListUseFilterBonusMyBalanceUseCheckBoxNotRegister()
    {
        RestartApp();
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Количество бонусов']")).click();

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/balanceFilterMyBalanceChbx")).isDisplayed();
    }


    
    @Test
    public void EventListUseFilterBonusMyBalanceUseCheckBoxDisabledSomething()
    {
        RestartApp();
        UserRegistration("9001111111");
        NavigateUp();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Впечатления']")).click();
        CloseEducationWindow();

        Timeout(5);
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/tabFiltLayout")).click();

        driver.findElement(By.xpath("//android.widget.TextView[@text='Количество бонусов']")).click();

        driver.findElement(By.id("ru.sberbank.spasibo.test:id/balanceFilterMyBalanceChbx")).click();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/minPriceText")).isDisplayed();
        driver.findElement(By.id("ru.sberbank.spasibo.test:id/maxPriceText")).isDisplayed();
    }

*/

    //--------------------------------------------------------------------------------------------------------------------------------------------------------

    //Использование конкретного параметра сортировки впечатлений
    private void UseSortEvent(String SortType, List<String> EthalonSortList)
    //SortType (string) - название сортировки (точное)
    //EthalonSortList (List) - какой лист ожидаем (порядок в листе = порядок элементов после сортировки)
    {
        By SortItem = By.xpath("//android.widget.TextView[@text='" + SortType + "']");
        SwipeForFindElement(SortItem);
        driver.findElement(SortItem).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        for(int i=0; i<EthalonSortList.size(); i++)
        {
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

            try {
                By currItemSpecialOffer = By.xpath("//android.widget.RelativeLayout[@index='" + i + "']/" +
                        "android.widget.LinearLayout/" +
                        "android.widget.LinearLayout/" +
                        "android.widget.LinearLayout/" +
                        "android.widget.TextView[@text='" + EthalonSortList.get(i) + "']");


                driver.findElement(currItemSpecialOffer);

            } catch (Exception e)
            {
                By currItemEvent = By.xpath("//android.widget.RelativeLayout[@index='" + i + "']/" +
                        "android.widget.LinearLayout/" +
                        "android.widget.RelativeLayout/" +
                        "android.widget.LinearLayout/" +
                        "android.widget.TextView[@text='" + EthalonSortList.get(i) + "']");
                driver.findElement(currItemEvent);
            }
        }
    }
}



